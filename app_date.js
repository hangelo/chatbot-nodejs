
// módulo para tratamento de data e hora

function c_datahora() {
	 
	 
	/* função que retorna a data e hora em modo DATE e muda para STRING */
	
	this.get_datahora_p = function( data ) {
		
		return this.get_datahora();
		
		// Guarda cada pedaço em uma variável
		
		var dia     = data.getDate();           // 1-31
		var dia_sem = data.getDay();            // 0-6 (zero=domingo)
		var mes     = data.getMonth();          // 0-11 (zero=janeiro)
		var ano2    = data.getYear();           // 2 dígitos
		var ano4    = data.getFullYear();       // 4 dígitos
		var hora    = data.getHours();          // 0-23
		var min     = data.getMinutes();        // 0-59
		var seg     = data.getSeconds();        // 0-59
		var mseg    = data.getMilliseconds();   // 0-999
		var tz      = data.getTimezoneOffset(); // em minutos
		
		
		// Formata a data e a hora (note o mês + 1)
		
		var str_data = dia + '/' + (mes+1) + '/' + ano4;
		var str_hora = hora + ':' + min + ':' + seg;
		
		return str_data + ' ' + str_hora;
	}
	
	
	// função que retorna a data e hora atual
	
	this.get_datahora = function() {
		var data = new Date();
		
		
		// Guarda cada pedaço em uma variável
		
		var dia     = data.getDate();           // 1-31
		var dia_sem = data.getDay();            // 0-6 (zero=domingo)
		var mes     = data.getMonth();          // 0-11 (zero=janeiro)
		var ano2    = data.getYear();           // 2 dígitos
		var ano4    = data.getFullYear();       // 4 dígitos
		var hora    = data.getHours();          // 0-23
		var min     = data.getMinutes();        // 0-59
		var seg     = data.getSeconds();        // 0-59
		var mseg    = data.getMilliseconds();   // 0-999
		var tz      = data.getTimezoneOffset(); // em minutos
		
		
		// Formata a data e a hora (note o mês + 1)
		
		var str_data = ( dia <= 9 ? '0' + dia : dia ) + '/' + ( mes + 1 <= 9 ? '0' + ( mes + 1 ) : ( mes + 1 ) ) + '/' + ano4;
		var str_hora = ( hora <= 9 ? '0' + hora : hora ) + ':' + ( min <= 9 ? '0' + min : min ) + ':' + ( seg <= 9 ? '0' + seg : seg ) + ',' + mseg;
		
		return str_data + ' ' + str_hora;
	}
	
	
	// função que retorna a data e hora atual em formato para ser um nome de arquivo
	
	this.get_datahora_fs = function() {
		var data = new Date();
		
		
		// Guarda cada pedaço em uma variável
		
		var dia     = data.getDate();           // 1-31
		var dia_sem = data.getDay();            // 0-6 (zero=domingo)
		var mes     = data.getMonth();          // 0-11 (zero=janeiro)
		var ano2    = data.getYear();           // 2 dígitos
		var ano4    = data.getFullYear();       // 4 dígitos
		var hora    = data.getHours();          // 0-23
		var min     = data.getMinutes();        // 0-59
		var seg     = data.getSeconds();        // 0-59
		var mseg    = data.getMilliseconds();   // 0-999
		var tz      = data.getTimezoneOffset(); // em minutos
	
	
		// Formata a data e a hora (note o mês + 1)
		
		var str_data = String( ano2 ).substr( 1, 2 ) + String( mes + 1 <= 9 ? '0' + ( mes + 1 ) : ( mes + 1 ) ) + String( dia <= 9 ? '0' + dia : dia );
		var str_hora = String( hora <= 9 ? '0' + hora : hora ) + String( min <= 9 ? '0' + min : min ) + String( seg <= 9 ? '0' + seg : seg );
		
		return str_data + '.' + str_hora;
	}
	
	
	// função que retorna a data e hora atual em formato para sql
	
	this.get_datahora_sql = function() {
		var data = new Date();
		
		
		// Guarda cada pedaço em uma variável
		
		var dia     = data.getDate();           // 1-31
		var dia_sem = data.getDay();            // 0-6 (zero=domingo)
		var mes     = data.getMonth();          // 0-11 (zero=janeiro)
		var ano2    = data.getYear();           // 2 dígitos
		var ano4    = data.getFullYear();       // 4 dígitos
		var hora    = data.getHours();          // 0-23
		var min     = data.getMinutes();        // 0-59
		var seg     = data.getSeconds();        // 0-59
		var mseg    = data.getMilliseconds();   // 0-999
		var tz      = data.getTimezoneOffset(); // em minutos
		
		
		// Formata a data e a hora (note o mês + 1)
		
		var str_data = ano4 + '-' + ( mes + 1 <= 9 ? '0' + ( mes + 1 ) : ( mes + 1 ) ) + '-' + ( dia <= 9 ? '0' + dia : dia );
		var str_hora = ( hora <= 9 ? '0' + hora : hora ) + ':' + ( min <= 9 ? '0' + min : min ) + ':' + ( seg <= 9 ? '0' + seg : seg ) + ',' + mseg;
		
		return str_data + ' ' + str_hora;
	}
	
};

module.exports = c_datahora;
