
// adiciona a biblioteca para manipulação de arquivos

var fs = require( 'fs' );


// adiciona cores ao CONSOLE.LOG

var colors = require( 'colors' );


// funções para manipulação de data e hora

var c_datahora = require( './app_date.js' );
var datahora = new c_datahora();


// classe para manipulação de arquivos de log

var c_log = function() {
	
	// escreve informações em arquivo de log
	/*  parâmetros:
			arq = nome do arquivo de log - (String)
			assunto = motivo do lot - (String)
			info = informação detalhada do evento - (string)
			arq_diff = TRUE se for arquivo separado por data/hora. FALSE se for mesmo arquivo incremental - (boolean)
			p_console = TRUE se for para exibir o log no console do sistema - (boolean)
	*/
	
	this.log_file = _log_file;
	function _log_file( arq, assunto, info, arq_diff, p_console ) {
		
		var v_datahora = datahora.get_datahora();
		var linha_info = v_datahora + ' GMT+0' + ' : ' + assunto + ' - ' + info;
		var c_info = linha_info + "\n";
		var f_info = linha_info + "\n";
		
		
		// escreve arquivo de lote
		
		fs.appendFile( constantes.DIR_LOGS + arq + ( arq_diff ? '.' + datahora.get_datahora_fs() : '' ) + '.log', f_info, function(err) { if ( err ) { console.log(err); throw err; } } )
		
		
		// exibe o log no terminal
		
		if ( p_console ) console.log( v_datahora + ' : ' + ( assunto + ' - ' ).toString().red + info );
	}
	
	
	// adiciona um evento de erro para um motivo específico. Sempre quando ocorre erro em algumas das funções de evento do socket
	
	this.log_file__app_err = _log_file__app_err;
	function _log_file__app_err( alog, arq_log1, arq_log2, func, err, data ) {
		
		var _log = '';
		_log += '' + "\n" + '----------------------------------------------------------------------------------------------------------------------' + "\n";
		_log += 'Erro em ' + func + '.' + "\n\n";
		_log += 'err:' + "\n" + err + "\n\n";
		_log += 'dump:' + "\n" + data + "\n";
		_log += '----------------------------------------------------------------------------------------------------------------------';
		alog.log_file( arq_log1, 'ERRO', _log, false, false );
		alog.log_file( arq_log2, 'ERRO', _log, false, false );
		
		console.log( _log );
		
	}	

}

module.exports = c_log;
