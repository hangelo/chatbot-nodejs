
/* controla as contantes do sistema */

var c_constantes = function () {

	this.ASK_TO_BOT = 'get-message-from-client';
	this.ANSWAR_TO_CLIENT = 'send-message-to-client';
	
	
	/* função que inicializa algumas das variáveis que dependam de parâmetros extras */
	
	this.init = _init;
	function _init() {
	}

}

module.exports = c_constantes;
