

// funções para manipulação de data e hora

var c_datahora = require( './app_date.js' );
var datahora = new c_datahora();


// contantes usadas no sistema

var c_constantes = require( './app_const.js' );
constantes = new c_constantes();


// adiciona a biblioteca para manipulação de arquivos

var fs = require( 'fs' );


// adiciona cores ao CONSOLE.LOG

var colors = require( 'colors' );


var c_dict = function () {

	this.questions = new Array();
	this.indexes = new Array();

	this.AddQuestion = _AddQuestion;
	function _AddQuestion(question, answer)
	{
		this.questions[this.questions.length] = new Array(question, answer);
	}

	this.AddIndex = _AddIndex;
	function _AddIndex(query, index)
	{
		this.indexes[this.indexes.length] = new Array(query, index);
	}

	this.FindQuestion = _FindQuestion;
	function _FindQuestion(question)
	{
		i = 0;
		qt = this.indexes.length;
		found = false;
		index = -1;

		while (!found && i < qt) {

			var reg = new RegExp(this.indexes[i][0], 'i');
			var search = question;

			if (search.match(reg)) {
				found = true;
				index = this.indexes[i][1];
			}
			else {
				i++;
			}
		}
		if (found) return this.questions[parseInt(index) - 1][1];
		else return false;
	}
}

var dict = new c_dict();


// Read the dictionary

var obj = JSON.parse(fs.readFileSync('dict.json', 'utf8'));
for (var i = 1; i <= 14; i++) {
	dict.AddQuestion(obj.questions[i].question, obj.questions[i].answer);
}

var keys = obj.keys;
for (var i = 0; i < keys.length; i++) {
	dict.AddIndex(obj.keys[i].query, obj.keys[i].index);
}


// continua daqui

var http = require( 'http' );
var url = require( 'url' );

var app = http.createServer( function( request, response ) {
	
	var pathname = url.parse( request.url ).pathname;
	
	fs.readFile( "index.html", "utf-8", function( error, data ) {
		response.writeHead( 200, { "Content-Type": "text/html" } );
		response.write( 'escreve: ' + data );
		response.end();
	} );
	
} ).listen( 2309 );


// carrega a classe IO

var io = require( "socket.io" ).listen( app );


// configura o IO

//io.enable( 'browser client minification' );
//io.enable( 'browser client etag' );
//io.enable( 'browser client gzip' );
io.set( 'transports', [ 'websocket', 'polling' ] );


/* controle dos eventos do socket cliente para o servidor */

io.sockets.on( "connection", function( socket ) {


	// pega informações de nova conexão e cria nodo para preencher vetor de conexões, mas ainda não insere no vetor
	/*
	var _conexao = new conexao();
	_conexao.ativa = true; // indicaque esta conexão está ativa
	_conexao.etapa_autenticacao = 1; // indica primeiro estágio, está conectado mas não autenticadi ainda
	_conexao.host = socket.handshake.headers.host;
	_conexao.origin = socket.handshake.headers.origin;
	_conexao.referer = socket.handshake.headers.referer;
	_conexao.useragent = socket.handshake.headers[ 'user-agent' ];
	_conexao.ip = socket.handshake.address;
	_conexao.id = socket.id;
	_conexao.nome = 'autenticando ...';
	_conexao.datahora_in = datahora.get_datahora();
	_conexao.datahora_out = null;
	_conexao.token = socket.handshake.query.token;
	*/

	console.log('connection');
	console.log(socket.handshake.headers);

	
	// conexão é fechada
	
	socket.on( 'disconnect', function() {
		var _id = socket.id;
		console.log('disconnect');
		return;
	});
	
	socket.on( constantes.ASK_TO_BOT, function( data ) {
		
		try {

			data = JSON.parse(data);
			var question = data.question;

			var answer = dict.FindQuestion(question);
			if (answer !== false) {
				io.to(socket.id).emit( constantes.ANSWAR_TO_CLIENT, answer );
			}
			else {
				io.to(socket.id).emit( constantes.ANSWAR_TO_CLIENT, 'repeat please' );
			}
			
		} catch( err ) {
			console.log('ERROR');
			console.log(err);
		}
	});
		

}); // finaliza objeto SOCKET

