<!DOCTYPE html>
<html>
<head>


	<title></title>


	<script src="socket.io.js"></script>


	<script>

		function CheckEnter(evt)
		{
			/** Check if Key clicked is ENTER key
			*/
			var key_code = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which ? evt.which : void 0;
			return ( key_code == 13 );
		}


		// Make a connection with bot

		socketio = io.connect( 'http://52.67.121.213:2309', { query: 'token=abc' } );
		

		// Make some event links for connections and disconnections

		socketio.on( 'connection', socket_connection );
		socketio.on( 'connect', socket_connect );
		socketio.on( 'disconnect', socket_disconnect );

		function socket_connection( data ) {
			console.log('socket_connection');
		}

		function socket_connect( data ) {
			console.log('socket_connect');
		}

		function socket_disconnect( data ) {
			console.log('socket_disconnect');
		}
		

		// Make a link between events from bot and related functions

		socketio.on( 'send-message-to-client', MessageFromBot );


		function MessageFromBot(data)
		{
			/**
			Get messages from bot
			*/
			AddMessageFromBotIntoBox(data);
		}


		function SendMessage()
		{
			/**
			Send a message to bot
			*/

			// get the message to send
			var mbj_msg_to_send = document.getElementById('msg_to_send');


			AddMessageFromClientIntoBox(mbj_msg_to_send.value);


			// foormat a JSON
			var msg = mbj_msg_to_send.value;
			var p = '{"question": "' + msg + '"}';
			
			// send to bot
			socketio.emit('get-message-from-client', p);

			// clear the input text for new messages
			mbj_msg_to_send.value = '';
		}


		function AddMessageFromClientIntoBox(message)
		{
			var str = '';
			str += '<div class="msg_from_client">'
				+ '<div class="msg">' + message + '</div>'
				+ '</div>';
			document.getElementById('messages').innerHTML += str;
			document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
		}


		function AddMessageFromBotIntoBox(message)
		{
			var str = '';
			str += '<div class="msg_from_bot">'
				+ '<div class="msg">' + message + '</div>'
				+ '</div>';
			document.getElementById('messages').innerHTML += str;
			document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
		}
	</script>


	<style>
		body {font-family:arial; font-size:12px; color:#464646;}

		.box {border:4px solid #DDD; border-radius:4px; padding:10px; margin:0 15%;}

		.tit {font-size:2em; text-align:center; margin:0 0 10px 0;}

		.msgs {height:300px; padding:10px; border:1px solid #DDD;}
		.msgs > .in {height:100%; overflow:auto;}

		.msg_from_client {background:#97FFFB; margin-left:15%;}
		.msg_from_bot {background:#A9E892; margin-right:15%;}

		.msg_from_client,
		.msg_from_bot {margin-top:15px; margin-bottom:15px; border-radius:4px; padding:4px;}

		.msg_from_client > .msg,
		.msg_from_bot > .msg {}

		.input {line-height:34px; height:34px; margin:10px 0 0 0;}
		.input > .txt {height:inherit; line-height:inherit; width:220px; float:left; border:1px solid #E0D4B2; padding:0 5px; outline:none !important;}
		.input > .btn {width:66px; float:right; background:#E0D4B2; border:1px solid #E0D4B2; border-radius:0 4px 4px 0; text-align:center; cursor:pointer;}
		.input input[type=text] {border:none; width:100%; height:100%; background:#FFF; padding:0; margin:0;}
		}
	</style>

</head>
<body>

	<div class="box">

		<div class="tit">Vanhackthon Agent Bot</div>

		<div class="msgs">
			<div class="in" id="messages">
			</div>
		</div>

		<div class="input">
			<div class="txt"><input type="text" id="msg_to_send" value="" placeholder="Leave your question here" onkeypress="javascript: if (CheckEnter(event)) {SendMessage();}"></div>
			<div class="btn" onclick="SendMessage();">Send</div>
			<div style="clear:both;"></div>
		</div>

	</div>

</body>
</html>